import sys

from antlr4 import *
from MxsLexer import MxsLexer
from MxsParser import MxsParser
from MxsVisitor import MxsVisitor

scope = 0
newlines = 0
block_def_state = False

# Parent Context : ctx.parentCtx

def indent():
    global scope
    return scope * "    "

class MyMxsVisitor(MxsVisitor):

    def visitProg(self, ctx):
        self.visitChildren(ctx)


    def visitExpr(self, ctx):
        # print("EXPR :'" + ctx.getText() + "'")
        self.visitChildren(ctx)


    def visitEmpty_line(self, ctx):
        print("")


    def visitSemicolon_sep(self, ctx):
        print("; ", end="")


    def visitAssignment(self, ctx):
        # print("ASSIGNMENT")
        global scope
        old_scope = scope
        print(indent() + ctx.destination().getText() + " = ", end="")
        scope = 0
        self.visit(ctx.expr())
        scope = old_scope


    def visitExpr_seq_line(self, ctx):
        # print("SEQ LINE")
        global scope
        old_scope = scope

        if not block_def_state:
            print(indent(), end="")

        print("(", end="")
        scope = 0 

        self.visitChildren(ctx)
        print(")", end="")
        scope = old_scope


    def visitExpr_seq_block(self, ctx):
        # print("SEQ BLOCK", end="")
        global scope, block_def_state
        indent_str = ""
        if not block_def_state:
            indent_str = indent()
        

        last_expr = ""

        child_count = 0

        for c in ctx.getChildren():
            child_count += 1

        # Correcting block start matching : (expr without \n
        if ctx.getChild(1).getText() != "\n":
            print(indent_str + "(")
        else:
            print(indent_str + "(", end="")

        scope += 1
        for i in range(child_count):
            c = ctx.getChild(i)
            self.visit(c)
            if i<(child_count-1):
                last_expr = c.getText()
        scope -= 1

        # Correcting block end matching : expr) without \n
        if last_expr != "\n":
            print("")

        print(indent() + ")", end="")
        # print("END SEQ BLOCK", end="")


    def visitDeclaration(self, ctx):
        print(ctx.VARNAME().getText(), end="")
        if ctx.expr():
            print(" = " + ctx.expr().getText(), end="")


    def visitVar_declaration(self, ctx):
        # print("DECL", end="")
        print(indent() + ctx.VARSCOPE().getText(), end=" ")
        count = 0
        # index 0 is VARSCOPE
        for c in ctx.getChildren():
            if count > 1:
                if c.getText() != ",":
                    print(", ", end="")
            self.visit(c)
            count += 1
        # print("END_DECL")


    def visitSimple_expr(self, ctx):
        for c in ctx.getChildren():
            self.visit(c)


    def visitOperand(self, ctx):
        child_count = 0
        for c in ctx.getChildren():
            child_count += 1
            self.visit(c)


    def visitIf_expr(self, ctx):
        # print("IF_CONTEXT")
        global scope, block_def_state
        old_scope = scope
        block_def_state = True

        test = ctx.if_test()
        expr = ctx.if_statement()
        print(indent() + "if ", end="")
        scope = 0
        self.visit(test)
        scope = old_scope
        print(" then ", end="")
        self.visit(expr)

        if ctx.else_statement():
            print("\n" + indent() + "else ", end="")
            self.visit(ctx.else_statement())
        # print("END_IF_CONTEXT")

    def visitFactor(self, ctx):
        child_count = 0
        for c in ctx.getChildren():
            child_count += 1
            if(isinstance(c, tree.Tree.TerminalNodeImpl)):
                 print(indent() + c.getText(), end="")
            else:
                self.visit(c)


    def visitFunction_def(self, ctx):
        global block_def_state
        block_def_state = True
        print(indent() + "fn " + ctx.VARNAME().getText(), end="")
        for c in ctx.function_arg():
            self.visit(c)
        print (" = ", end="")
        self.visit(ctx.expr())
        block_def_state = False


    def visitFunction_arg(self, ctx):
        print(" " + ctx.getText(), end="")
        
  

    def visitFunction_call(self, ctx):
        # print("FUNC CALL")
        print(indent() + ctx.operand().getText(), end="")

        param_count = 0
        for c in ctx.parameter():
            param_count += 1
            self.visit(c)

        if param_count == 0:
            print("()", end="")
        else:
            print("")
        # print("END FUNC CALL")


    def visitParameter(self, ctx):
        if ctx.VARNAME():
            print(" " + ctx.VARNAME().getText() + ":" + ctx.operand().getText(), end="")
        else:
            print(" " + ctx.operand().getText(), end="")



def main(argv):
    input_stream = FileStream(argv[1])
    lexer = MxsLexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = MxsParser(stream)
    tree = parser.prog()
    ast = MyMxsVisitor().visitProg(tree)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("USAGE : test.py sample.ms")
        exit(0)
    main(sys.argv)
