all:
	antlr4 -Dlanguage=Python3 Mxs.g4 -visitor

clean:
	rm -f *.interp
	rm -f *.tokens
	rm -f Mxs*.py
	rm -rf __pycache__
