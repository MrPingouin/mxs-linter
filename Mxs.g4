// Antlr convention : 
// minorcase_start : parser rule
// UPPERCASE_start : lexer rule

grammar Mxs;

prog 
    : expr* EOF 
    ;

expr 
    : simple_expr
    | var_declaration
    | assignment
    | if_expr
    | function_def 
	| empty_line
    ;

empty_line
	: '\n'
	;

simple_expr 
    : function_call
    | operand 
    | expr_seq_line
    | expr_seq_block 
    ;

expr_seq_block
    : '(' expr expr* ')' 
    ;


expr_seq_line
    : '(' expr (semicolon_sep expr )* ')' 
    ;

semicolon_sep
    : ';'
    ;

if_expr
    : 'if' if_test 'then' '\n'? if_statement ('else' else_statement)?
    | 'if' if_test 'do' if_statement
    ;

// To simplify code formatting
if_test: expr ;
if_statement: expr ;
else_statement: expr ;


var_declaration 
    : VARSCOPE declaration (',' declaration)* 
    ;

declaration 
    : VARNAME ( '=' expr )? 
    ;

assignment 
    : destination '=' expr 
    ;

operand 
    : factor 
    ;

factor 
    : VARNAME 
    | BOOL 
    | NUMBER 
    ;

destination 
    : VARNAME 
    ;

function_def 
    : MAPPED_FN? FUNCTION VARNAME function_arg* '=' '\n'? expr 
    ;

function_arg 
    : VARNAME
    | VARNAME ':' operand? 
    ;

function_call 
    : operand '(' ')'
    | operand parameter+ '\n'? //{print("LT(1) : ", self._input.LT(1).text)}
    ;

parameter 
    : operand 
    | VARNAME ':' operand 
    ;


MAPPED_FN 
    : 'mapped' 
    ;

FUNCTION 
    : 'function' 
    | 'fn' 
    ;

VARSCOPE 
    : 'local' 
    | 'global' 
    ;

BOOL 
    : 'true' 
    | 'false' 
    ;

VARNAME 
    : [a-zA-Z_]+ 
    ;

NUMBER 
    : [-]?[0-9]+ 
    ;

WHITESPACE 
    : [ \t]+ -> skip
    ;

// For futures rules
//ifStmt : ’if’ ID stmt (’else’ stmt | {self.input.LA(1) != ELSE}?);
