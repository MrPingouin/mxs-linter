# Mxs-Linter

Antlr4-based maxscript linter.

As the objective is to format source code, grammar in mxs-linter and in official maxscript documentation are a little bit different.

## Building the parser

```
make 
```

## Testing the tool

```
python3 test.py sample.ms
```

## Notes

Antlr4-python3-runtime is not available on Debian 10 (Jessie).

- [Antlr4-python3-runtime](https://pypi.org/project/antlr4-python3-runtime/#files)
- [Maxscript grammar](https://help.autodesk.com/view/3DSMAX/2020/ENU/?guid=GUID-46ECBAD7-35E9-43BD-8A8C-849772924805)


## Example

Very bad formatted code.

```lua
(
local    my_var  =    100

function f x y:42 = (42
true)

fn toto a     b:0 = ( false; false)

toto()
toto      a  b:12
true

    if true then 
		(
        false
        true
    )
)

```

```lua
(
    local my_var = 100

    fn f x y:42 = (
        42
        true
    )

    fn toto a b:0 = (false; false)

    toto()

    toto a b:12
    true

    if true then (
        false
        true
    )
)

```
